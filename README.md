GHT Mojang API Client
=========================

The Mojang API Client provides methods to access the [Mojang API](http://wiki.vg/Mojang_API) in PHP applications.  For API methods that require it, the client also provides authentication via the [Mojang Authentication API](http://wiki.vg/Authentication).

# Installation

## Get the Composer package

To install with [Composer](https://getcomposer.org/), run `composer require greenhollowtech/ght-mojang-api-client`.

# Usage

To create an instance of the client:

```php
use GHT\MojangApiClient\GHTMojangApiClient;

// Instantiate the client
$client = GHTMojangApiClient::createApiClient();
$client = GHTMojangApiClient::createAuthenticationClient();
$client = GHTMojangApiClient::createSessionClient();
$client = GHTMojangApiClient::createStatusClient();

// The client can be be instantiated the long way
$targetApi = GHTMojangApiClient::SUBDOMAIN_API;
$client = GHTMojangApiClient::create($targetApi);

// Other targets
$targetApi = GHTMojangApiClient::SUBDOMAIN_OAUTH;
$targetApi = GHTMojangApiClient::SUBDOMAIN_SESSION;
$targetApi = GHTMojangApiClient::SUBDOMAIN_STATUS;
```

Then use the client to call any of the related methods for that client, described below.

# API Methods

## Basic API

```php
$client = GHTMojangApiClient::createApiClient();

// Get all names a user has ever used
$names = $client->getNames($uuid);

// Get the UUID for the user currently or in the past using a given name
$profile = $client->getProfileForName('SomeDude');
$profile = $client->getProfileForName('SomeDude', new \DateTime('-1 month'));

// Get the UUIDs (and possibly other limited information) of a list of names
$profiles = $client->getProfilesForNames(array('SomeDude', 'SomeOtherDude'));

// Get Mojang statistics
$statistics = $client->getStatistics();
$metrics = array('item_sold_minecraft', 'item_sold_scrolls');
$statistics = $client->getStatistics($metrics);
$splitByMetric = true;
$statistics = $client->getStatistics($metrics, $splitByMetric);
```

## Authentication API

```php
use GHT\MojangApiClient\Exception\MojangApiException;

$authClient = GHTMojangApiClient::createAuthenticationClient();

try {
    // Client will transport the credentials to use with other API requests
    $authClient->authenticate($email, $password);

    // Authenticating with a game as the agent provides better credentials
    $authClient->authenticate($email, $password, 'Minecraft');

    // Refreshes the API token transported in the client
    $authClient->refresh();

    // Will throw an exception if not valid, otherwise returns nothing
    $authClient->validate();

    // End the authenticated session by invalidating the token
    $authClient->invalidate();

    // End the authenticated session using the login name and password
    $authClient->signOut($email, $password);
}
catch (MojangApiException $e) {
    echo $e->getMessage();
}
```

## Basic API With Authentication

```php
use GHT\MojangApiClient\Exception\MojangApiException;

// Use an authentication client to get the credentials for the API client
$authClient = GHTMojangApiClient::createAuthenticationClient();
try {
    $authClient->authenticate($email, $password);
}
catch (MojangApiException $e) {
    // login failed, go somewhere else
}
$client = GHTMojangApiClient::createApiClient($authClient->getCredentials());

// Change a skin
try {
    $client->changeSkin($uuid, $url, 'slim');
}
catch (MojangApiExceptin $e) {
    echo $e->getMessage();
}

// Get the current user's information
$userInfo = $client->getUserInformation();

// Reset the user's skin
try {
    $client->resetSkin($uuid);
}
catch (MojangApiExceptin $e) {
    echo $e->getMessage();
}

// Upload a skin
try {
    $client->uploadSkin($uuid, '/tmp/skinFile.png', 'slim');
}
catch (MojangApiExceptin $e) {
    echo $e->getMessage();
}
```

## Session API

```php
$client = GHTMojangApiClient::createSessionClient();

// Get blocked server hashes
$hashes = $client->getBlockedServerHashes();
$serverIsBlocked = in_array(sha1('*.myservername.com'), $hashes);

// Get profile data for a UUID
$profile = $client->getProfile($uuid);
```

## Status API

```php
// Get statuses for all services
$statuses = $client->getStatus();
$minecraftStatus = $statuses['minecraft.net'];
```
