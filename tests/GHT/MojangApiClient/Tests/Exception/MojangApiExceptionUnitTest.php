<?php

namespace GHT\MojangApiClient\Tests\Exception;

use GHT\MojangApiClient\Exception\MojangApiException;

/**
 * Exercises the Mojang API Exception.
 */
class MojangApiExceptionUnitTest extends \PHPUnit_Framework_TestCase
{
    /**
     * {@inheritdoc}
     */
    protected function setUp()
    {
    }

    /**
     * {@inheritdoc}
     */
    protected function tearDown()
    {
    }

    /**
     * Verify that an exception can be instantiated without a message.
     */
    public function testConstructNoMessage()
    {
        $exception = new MojangApiException();
        $this->assertEquals('', $exception->getMessage());
    }

    /**
     * Verify that an exception can be instantiated with the expected API error
     * JSON parsed for the message.
     */
    public function testConstructJsonErrorMessage()
    {
        $jsonError = json_encode(array('error' => 'Test Error', 'errorMessage' => 'A test error occurred'));
        $exception = new MojangApiException($jsonError);
        $this->assertEquals('Test Error: A test error occurred', $exception->getMessage());
    }

    /**
     * Verify that an exception can be instantiated with unexpected API response
     * JSON that can't be parsed for the message.
     */
    public function testConstructJsonUnexpectedResponse()
    {
        $jsonError = json_encode(array('unexpected' => 'Test Unexpected', 'data' => 'Unexpected data response'));
        $exception = new MojangApiException($jsonError);
        $this->assertEquals('Unknown Error: API response :: ' . $jsonError, $exception->getMessage());
    }
}
