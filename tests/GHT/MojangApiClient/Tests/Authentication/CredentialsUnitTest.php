<?php

namespace GHT\MojangApiClient\Tests\Authentication;

use GHT\MojangApiClient\Authentication\Credentials;

/**
 * Exercises the Mojang API authentication Credentials.
 */
class CredentialsUnitTest extends \PHPUnit_Framework_TestCase
{
    /**
     * {@inheritdoc}
     */
    protected function setUp()
    {
    }

    /**
     * {@inheritdoc}
     */
    protected function tearDown()
    {
    }

    /**
     * Verify that a new instance has a client token automatically set.
     */
    public function testConstruct()
    {
        $credentials = new Credentials();
        $this->assertRegExp('/^[a-z0-9]{40}$/', $credentials->getClientToken());
    }
}
