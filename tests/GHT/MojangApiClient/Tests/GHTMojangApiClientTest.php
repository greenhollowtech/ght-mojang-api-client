<?php

namespace GHT\MojangApiClient\Tests;

use GHT\MojangApiClient\Authentication\Credentials;
use GHT\MojangApiClient\Exception\MojangApiException;
use GHT\MojangApiClient\GHTMojangApiClient;
use phpmock\phpunit\PHPMock;

/**
 * Exercises the GHT Mojang API Client.
 */
class GHTMojangApiClientUnitTest extends \PHPUnit_Framework_TestCase
{
    use PHPMock;

    /**
     * @var array
     */
    protected $mockFunctions;

    /**
     * {@inheritdoc}
     */
    protected function setUp()
    {
        // Configure all the expected PHP functions
        $this->mockFunctions = array();
        foreach (array(
            'curl_close',
            'curl_exec',
            'curl_getinfo',
            'curl_init',
            'curl_setopt',
            'fseek',
            'fwrite',
            'tmpfile',
        ) as $functionName) {
            $this->mockFunctions[$functionName] = $this->getFunctionMock('GHT\ApiClient', $functionName);
        }
    }

    /**
     * {@inheritdoc}
     */
    protected function tearDown()
    {
        unset($this->mockFunctions);
    }

    /**
     * Verify that a UUID can be cleaned.
     */
    public function testCleanUuid()
    {
        $this->assertEquals(
            '1test234uuid567890',
            GHTMojangApiClient::cleanUuid('1test234-uuid5-67890')
        );
    }

    /**
     * Verify that we can get a list of all valid API subdomains.  Used by the
     * create method, so test this first.
     */
    public function testGetApiNames()
    {
        $this->assertEquals(
            array('api', 'authserver', 'sessionserver', 'status'),
            GHTMojangApiClient::getApiNames()
        );
    }

    /**
     * Verify that we can create a client instance.
     */
    public function testCreate()
    {
        $client = GHTMojangApiClient::create('api');

        // Verify the client object
        $this->assertEquals('GHT\MojangApiClient\GHTMojangApiClient', get_class($client));

        // Verify the type of connector
        $reflectionClass = new \ReflectionClass($client);
        $connectorProperty = $reflectionClass->getProperty('connector');
        $connectorProperty->setAccessible(true);
        $connector = $connectorProperty->getValue($client);

        $this->assertEquals('GHT\ApiClient\Entity\ApiConnector', get_class($connector));
    }

    /**
     * Verify that we can create a client instance with OAuth2 authentication.
     */
    public function testCreateWithOauthToken()
    {
        $credentials = new Credentials();
        $credentials->setToken('testOauthToken');
        $client = GHTMojangApiClient::create('api', $credentials);

        // Verify the client object
        $this->assertEquals('GHT\MojangApiClient\GHTMojangApiClient', get_class($client));

        // Verify the type of connector
        $reflectionClass = new \ReflectionClass($client);
        $connectorProperty = $reflectionClass->getProperty('connector');
        $connectorProperty->setAccessible(true);
        $connector = $connectorProperty->getValue($client);

        $this->assertEquals('GHT\ApiClient\Entity\OauthConnector', get_class($connector));
    }

    /**
     * Verify that attempting to create a client instance with an invalid API
     * subdomain name.
     */
    public function testCreateInvalidApiName()
    {
        // Expect the exception
        $this->expectException(\InvalidArgumentException::class);
        $this->expectExceptionMessage('"bogus" is not a valid API subdomain name.  Expected one of:');

        // Trigger the exception
        $client = GHTMojangApiClient::create('bogus');
    }

    /**
     * Verify that we can create a default API client instance.
     */
    public function testCreateApiClient()
    {
        $client = GHTMojangApiClient::createApiClient();

        // Verify the client object
        $this->assertEquals('GHT\MojangApiClient\GHTMojangApiClient', get_class($client));

        // Verify the type of connector
        $reflectionClass = new \ReflectionClass($client);
        $connectorProperty = $reflectionClass->getProperty('connector');
        $connectorProperty->setAccessible(true);
        $connector = $connectorProperty->getValue($client);

        $this->assertEquals('GHT\ApiClient\Entity\ApiConnector', get_class($connector));
    }

    /**
     * Verify that we can create a default API client instance with OAuth2
     * authentication.
     */
    public function testCreateApiClientWithOauthToken()
    {
        $credentials = new Credentials();
        $credentials->setToken('testOauthToken');
        $client = GHTMojangApiClient::createApiClient($credentials);

        // Verify the client object
        $this->assertEquals('GHT\MojangApiClient\GHTMojangApiClient', get_class($client));

        // Verify the type of connector
        $reflectionClass = new \ReflectionClass($client);
        $connectorProperty = $reflectionClass->getProperty('connector');
        $connectorProperty->setAccessible(true);
        $connector = $connectorProperty->getValue($client);

        $this->assertEquals('GHT\ApiClient\Entity\OauthConnector', get_class($connector));
    }

    /**
     * Verify that we can create an authentication API client to provide
     * credentials for other API requests.  Also verifies the auto-generation of
     * the credentials.
     */
    public function testCreateAuthenticationClientAndCredentials()
    {
        $client = GHTMojangApiClient::createAuthenticationClient();

        // Verify that credentials have been auto-generated
        $credentials = $client->getCredentials();
        $this->assertEquals('GHT\MojangApiClient\Authentication\Credentials', get_class($credentials));
        $this->assertRegExp('/^[a-z0-9]{40}$/', $credentials->getClientToken());
    }

    /**
     * Verify that we can create a session API client instance.
     */
    public function testCreateSessionClient()
    {
        $client = GHTMojangApiClient::createSessionClient();

        // Verify the client object
        $this->assertEquals('GHT\MojangApiClient\GHTMojangApiClient', get_class($client));

        // Verify the type of connector
        $reflectionClass = new \ReflectionClass($client);
        $connectorProperty = $reflectionClass->getProperty('connector');
        $connectorProperty->setAccessible(true);
        $connector = $connectorProperty->getValue($client);

        $this->assertEquals('GHT\ApiClient\Entity\ApiConnector', get_class($connector));
    }

    /**
     * Verify that we can create a status API client instance.
     */
    public function testCreateStatusClient()
    {
        $client = GHTMojangApiClient::createStatusClient();

        // Verify the client object
        $this->assertEquals('GHT\MojangApiClient\GHTMojangApiClient', get_class($client));

        // Verify the type of connector
        $reflectionClass = new \ReflectionClass($client);
        $connectorProperty = $reflectionClass->getProperty('connector');
        $connectorProperty->setAccessible(true);
        $connector = $connectorProperty->getValue($client);

        $this->assertEquals('GHT\ApiClient\Entity\ApiConnector', get_class($connector));
    }

    /**
     * Verify that methods can validate the API name they require against the
     * current API name.  The rest of the API caller methods use this, so test
     * this first.
     */
    public function testValidateApi()
    {
        $client = GHTMojangApiClient::create('status');

        // Confirm that no exception is thrown
        $client->validateApi('status');
        $this->assertTrue(true);
    }

    /**
     * Verify that methods attempting to validate the API name they require
     * against the current mismatched API name will throw an exception.
     */
    public function testValidateApiWhenInvalid()
    {
        // Expect the exception
        $this->expectException(\BadMethodCallException::class);
        $this->expectExceptionMessage('"testValidateApiWhenInvalid" is not a method of the https://status.mojang.com API.');

        $client = GHTMojangApiClient::create('status');

        // Trigger the exception
        $client->validateApi('api');
    }

    /**
     * Verify that methods attempting to validate the API with an OAuth2 token
     * required will throw an exception when using the wrong connector type.
     */
    public function testValidateApiWithWrongConnectorType()
    {
        // Expect the exception
        $this->expectException(\BadMethodCallException::class);
        $this->expectExceptionMessage('The "testValidateApiWithWrongConnectorType" method requires an authentication token.');

        $client = GHTMojangApiClient::create('api');

        // Trigger the exception
        $client->validateApi('api', true);
    }

    /**
     * Verify that we can get a list of valid game names.  The authenticate
     * method uses this, so test this first.
     */
    public function testGetGameAgents()
    {
        $this->assertEquals(
            array('Minecraft', 'Scrolls'),
            GHTMojangApiClient::getGameAgents()
        );
    }

    /**
     * Verify that authentication can be requested.
     */
    public function testAuthenticate()
    {
        // Mock the API result
        $this->mockFunctions['curl_exec']->expects($this->once())
            ->will($this->returnValue('{"accessToken":"testAccessToken","clientToken":"testClientToken","selectedProfile":{"id":"testUuid","name":"testName"}}'))
        ;
        $this->mockFunctions['curl_getinfo']->expects($this->once())
            ->will($this->returnValue(200))
        ;

        // Get the client
        $client = GHTMojangApiClient::create(GHTMojangApiClient::SUBDOMAIN_OAUTH);

        // Verify the expected result
        $this->assertEquals(
            array(
                'accessToken' => 'testAccessToken',
                'clientToken' => 'testClientToken',
                'selectedProfile' => array(
                    'id' => 'testUuid',
                    'name' => 'testName',
                ),
            ),
            $client->authenticate('test@email.com', 'testPassword', 'Minecraft')
        );

        // Verify the credentials were configured
        $credentials = $client->getCredentials();
        $this->assertEquals('testUuid', $credentials->getProfileId());
        $this->assertEquals('testName', $credentials->getProfileName());
        $this->assertEquals('testAccessToken', $credentials->getToken());
    }

    /**
     * Verify that a failed authentication attempt throws an exception.
     */
    public function testAuthenticateFailure()
    {
        // Expect the exception
        $this->expectException(MojangApiException::class);
        $this->expectExceptionMessage('Invalid credentials. Invalid username or password.');

        // Mock the API result
        $this->mockFunctions['curl_exec']->expects($this->once())
            ->will($this->returnValue('{"error":"ForbiddenOperationException","errorMessage":"Invalid credentials. Invalid username or password."}'))
        ;
        $this->mockFunctions['curl_getinfo']->expects($this->once())
            ->will($this->returnValue(403))
        ;

        // Get the client
        $client = GHTMojangApiClient::create(GHTMojangApiClient::SUBDOMAIN_OAUTH);

        // Trigger the exception
        $client->authenticate('test@email.com', 'testWrongPassword');
    }

    /**
     * Verify that authentication can be requested with game agency.
     */
    public function testAuthenticateWithGameAgent()
    {
        // Mock the API result
        $this->mockFunctions['curl_exec']->expects($this->once())
            ->will($this->returnValue('{"accessToken":"testAccessToken","clientToken":"testClientToken","selectedProfile":{"id":"testUuid","name":"testName"},"availableProfiles":[{"id":"testUuid","name":"testName"}]}'))
        ;
        $this->mockFunctions['curl_getinfo']->expects($this->once())
            ->will($this->returnValue(200))
        ;

        // Get the client
        $client = GHTMojangApiClient::create(GHTMojangApiClient::SUBDOMAIN_OAUTH);

        // Verify the expected result
        $this->assertEquals(
            array(
                'accessToken' => 'testAccessToken',
                'clientToken' => 'testClientToken',
                'selectedProfile' => array(
                    'id' => 'testUuid',
                    'name' => 'testName',
                ),
                'availableProfiles' => array(
                    array(
                        'id' => 'testUuid',
                        'name' => 'testName',
                    ),
                ),
            ),
            $client->authenticate('test@email.com', 'testPassword', 'Minecraft')
        );
        $this->assertEquals('testUuid', $client->getCredentials()->getProfileId());
        $this->assertEquals('testName', $client->getCredentials()->getProfileName());
    }

    /**
     * Verify that a failed authentication attempt throws an exception.
     */
    public function testAuthenticateWithInvalidGameAgent()
    {
        // Expect the exception
        $this->expectException(\InvalidArgumentException::class);
        $this->expectExceptionMessage('"Checkers" is not a valid game name.  Expected one of:');

        // Get the client
        $client = GHTMojangApiClient::create(GHTMojangApiClient::SUBDOMAIN_OAUTH);

        // Trigger the exception
        $client->authenticate('test@email.com', 'testPassword', 'Checkers');
    }

    /**
     * Verify that we can get a list of valid skin model names.  The change skin
     * method uses this, so test this first.
     */
    public function testGetSkinModelNames()
    {
        $this->assertEquals(
            array('slim', 'standard'),
            GHTMojangApiClient::getSkinModelNames()
        );
    }

    /**
     * Verify that a skin change can be requested.
     */
    public function testChangeSkin()
    {
        // Get the client
        $credentials = new Credentials();
        $credentials->setToken('testOauthToken');
        $client = GHTMojangApiClient::create(GHTMojangApiClient::SUBDOMAIN_API, $credentials);

        // Confirm that no exception is thrown
        $client->changeSkin('testUUID', 'https://test.greenhollowtech.com/skins/test.png', 'slim');
        $this->assertTrue(true);
    }

    /**
     * Verify that attempting a skin change request with an invalid model name
     * throws an exception.
     */
    public function testChangeSkinWithInvalidModel()
    {
        // Expect the exception
        $this->expectException(\InvalidArgumentException::class);
        $this->expectExceptionMessage('"smooth" is not a valid skin model name.  Expected one of:');

        // Get the client
        $credentials = new Credentials();
        $credentials->setToken('testOauthToken');
        $client = GHTMojangApiClient::create(GHTMojangApiClient::SUBDOMAIN_API, $credentials);

        // Trigger the exception
        $client->changeSkin('testUUID', 'https://test.greenhollowtech.com/skins/test.png', 'smooth');
    }

    /**
     * Verify that attempting a skin change request with an invalid connector
     * throws an exception.
     */
    public function testChangeSkinWithoutOauth()
    {
        // Expect the exception
        $this->expectException(\BadMethodCallException::class);
        $this->expectExceptionMessage('The "changeSkin" method requires an authentication token.');

        // Get the client
        $client = GHTMojangApiClient::create(GHTMojangApiClient::SUBDOMAIN_API);

        // Trigger the exception
        $client->changeSkin('testUUID', 'https://test.greenhollowtech.com/skins/test.png', 'slim');
    }

    /**
     * Verify that credentials won't be overwritten once generated.
     */
    public function testGenerateCredentialsMaintainsOriginal()
    {
        $client = GHTMojangApiClient::createAuthenticationClient();

        // Verify that credentials are only generated once
        $clientToken1 = $client->getCredentials()->getClientToken();
        $client->generateCredentials();
        $clientToken2 = $client->getCredentials()->getClientToken();
        $this->assertEquals($clientToken1, $clientToken2);
    }

    /**
     * Verify that we can get a list of blocked server hashes.
     */
    public function testGetBlockedServerHashes()
    {
        // Mock the API result
        $this->mockFunctions['curl_exec']->expects($this->once())
            ->will($this->returnValue("testHash1\ntestHash2\ntestHash3"))
        ;

        // Get the client
        $client = GHTMojangApiClient::create(GHTMojangApiClient::SUBDOMAIN_SESSION);

        // Verify the expected result
        $this->assertEquals(
            array('testHash1', 'testHash2', 'testHash3'),
            $client->getBlockedServerHashes()
        );
    }

    /**
     * Verify that all names a profile ever used can be requested for a user by
     * UUID.
     */
    public function testGetNames()
    {
        // Mock the API result
        $this->mockFunctions['curl_exec']->expects($this->once())
            ->will($this->returnValue('[{"name":"testCurrentName"},{"name":"testPreviousName","changedAt":969105600}]'))
        ;

        // Get the client
        $client = GHTMojangApiClient::create(GHTMojangApiClient::SUBDOMAIN_API);

        // Verify the expected result
        $this->assertEquals(
            array(
                array('name' => 'testCurrentName'),
                array('name' => 'testPreviousName', 'changedAt' => '969105600'),
            ),
            $client->getNames('testUuid')
        );
    }

    /**
     * Verify that profile data can be requested for a user by UUID.
     */
    public function testGetProfile()
    {
        // Mock the API result
        $this->mockFunctions['curl_exec']->expects($this->once())
            ->will($this->returnValue(sprintf(
                '{"id":"testUuid","name":"testName","properties":[{"name":"textures","value":"%s"}]}',
                base64_encode('{"timestamp":213019200,"profileId":"testUuid","profileName":"testName","signatureRequired":true,"textures":{}}')
            )))
        ;

        // Get the client
        $client = GHTMojangApiClient::create(GHTMojangApiClient::SUBDOMAIN_SESSION);

        // Verify the expected result
        $this->assertEquals(
            array(
                'id' => 'testUuid',
                'name' => 'testName',
                'properties' => array(
                    array(
                        'name' => 'textures',
                        'value' => array(
                            'timestamp' => '213019200',
                            'profileId' => 'testUuid',
                            'profileName' => 'testName',
                            'signatureRequired' => true,
                            'textures' => array(),
                        )
                    ),
                )
            ),
            $client->getProfile('testUuid')
        );
    }

    /**
     * Verify that a UUID and other profile data can be requested for a user by
     * name at a specified time.
     */
    public function testGetProfileForName()
    {
        // Mock the API result
        $this->mockFunctions['curl_exec']->expects($this->once())
            ->will($this->returnValue('{"id":"testUuid","name":"testCurrentName"}'))
        ;

        // Get the client
        $client = GHTMojangApiClient::create(GHTMojangApiClient::SUBDOMAIN_API);

        // Verify the expected result
        $this->assertEquals(
            array('id' => 'testUuid', 'name' => 'testCurrentName'),
            $client->getProfileForName('testPreviousName', new \DateTime('-1 month'))
        );
    }

    /**
     * Verify that a UUID and other profile data can be requested for a user by
     * name without specifying a time.
     */
    public function testGetProfileForNameCurrentDateTime()
    {
        // Mock the API result
        $this->mockFunctions['curl_exec']->expects($this->once())
            ->will($this->returnValue('{"id":"testUuid","name":"testCurrentName"}'))
        ;

        // Get the client
        $client = GHTMojangApiClient::create(GHTMojangApiClient::SUBDOMAIN_API);

        // Verify the expected result
        $this->assertEquals(
            array('id' => 'testUuid', 'name' => 'testCurrentName'),
            $client->getProfileForName('testCurrentName')
        );
    }

    /**
     * Verify that attempting to request profile information for a user by name
     * with an invalid date time throws an exception.
     */
    public function testGetProfileForNameInvalidDateTime()
    {
        // Expect the exception
        $this->expectException(\InvalidArgumentException::class);
        $this->expectExceptionMessage('Attempting to get profile for name, expected DateTime object, got string.');

        // Get the client
        $client = GHTMojangApiClient::create(GHTMojangApiClient::SUBDOMAIN_API);

        // Trigger the exception
        $client->getProfileForName('testPreviousName', '1976-10-01');
    }

    /**
     * Verify that UUIDs and other profile data can be requested by user names,
     * with invalid names stripped out.
     */
    public function testGetProfilesForNames()
    {
        // Mock the API result
        $this->mockFunctions['curl_exec']->expects($this->once())
            ->will($this->returnValue('[{"id":"testUuid1","name":"testName1"},{"id":"testUuid2","name":"testName2"}]'))
        ;

        // Expect the data set to the post fields to be clean
        $this->mockFunctions['curl_setopt']->expects($this->any())
            ->with(
                $this->anything(),
                $this->anything(),
                $this->callback(function($value) {
                    // If the value is json, validate
                    if (!is_string($value)) {
                        return true;
                    }
                    $decodedValue = json_decode($value);
                    if (json_last_error() === JSON_ERROR_NONE
                        && !preg_match("/^\d+$/", trim($value))
                        && $value !== '{"0":"testName1","2":"testName2"}'
                    ) {
                        return false;
                    }

                    return true;
                })
            )
        ;

        // Get the client
        $client = GHTMojangApiClient::create(GHTMojangApiClient::SUBDOMAIN_API);

        // Verify the expected result
        $this->assertEquals(
            array(
                array('id' => 'testUuid1', 'name' => 'testName1'),
                array('id' => 'testUuid2', 'name' => 'testName2'),
            ),
            $client->getProfilesForNames(array('testName1', null, 'testName2', ''))
        );
    }

    /**
     * Verify that we can get a list of all valid statistics metrics keys.  Used
     * by the getStatistics method, so test this first.
     */
    public function testGetStatisticsKeys()
    {
        $this->assertEquals(
            array('item_sold_cobalt', 'prepaid_card_redeemed_minecraft', 'item_sold_minecraft', 'item_sold_scrolls'),
            GHTMojangApiClient::getStatisticsKeys()
        );
    }

    /**
     * Verify that we can request all statistics as an aggregate.
     */
    public function testGetStatisticsAggregateOfAll()
    {
        // Mock the API result
        $this->mockFunctions['curl_exec']->expects($this->once())
            ->will($this->returnValue('{"total":12345678,"last24h":6789,"saleVelocityPerSeconds":0.098765432}'))
        ;

        // Get the client
        $client = GHTMojangApiClient::create(GHTMojangApiClient::SUBDOMAIN_API);

        // Verify the expected result
        $this->assertEquals(
            array('total' => '12345678', 'last24h' => '6789', 'saleVelocityPerSeconds' => '0.098765432'),
            $client->getStatistics()
        );
    }

    /**
     * Verify that we can request selected statistics as an aggregate.
     */
    public function testGetStatisticsAggregateOfSelected()
    {
        // Mock the API result
        $this->mockFunctions['curl_exec']->expects($this->once())
            ->will($this->returnValue('{"total":12345678,"last24h":6789,"saleVelocityPerSeconds":0.098765432}'))
        ;

        // Get the client
        $client = GHTMojangApiClient::create(GHTMojangApiClient::SUBDOMAIN_API);

        // Verify the expected result
        $this->assertEquals(
            array('total' => '12345678', 'last24h' => '6789', 'saleVelocityPerSeconds' => '0.098765432'),
            $client->getStatistics(array('item_sold_cobalt', 'item_sold_scrolls'))
        );
    }

    /**
     * Verify that we can request all statistics split into separate requests.
     */
    public function testGetStatisticsAllSplit()
    {
        // Mock the API result
        $this->mockFunctions['curl_exec']->expects($this->exactly(4))
            ->will($this->onConsecutiveCalls(
                '{"total":1234,"last24h":123,"saleVelocityPerSeconds":0.012345678}',
                '{"total":5678,"last24h":456,"saleVelocityPerSeconds":0.066666667}',
                '{"total":12345678,"last24h":6789,"saleVelocityPerSeconds":0.098765432}',
                '{"total":123456,"last24h":789,"saleVelocityPerSeconds":0.033333333}'
            ))
        ;

        // Get the client
        $client = GHTMojangApiClient::create(GHTMojangApiClient::SUBDOMAIN_API);

        // Verify the expected result
        $this->assertEquals(
            array(
                'item_sold_cobalt' => array('total' => '1234', 'last24h' => '123', 'saleVelocityPerSeconds' => '0.012345678'),
                'prepaid_card_redeemed_minecraft' => array('total' => '5678', 'last24h' => '456', 'saleVelocityPerSeconds' => '0.066666667'),
                'item_sold_minecraft' => array('total' => '12345678', 'last24h' => '6789', 'saleVelocityPerSeconds' => '0.098765432'),
                'item_sold_scrolls' => array('total' => '123456', 'last24h' => '789', 'saleVelocityPerSeconds' => '0.033333333'),
            ),
            $client->getStatistics(null, true)
        );
    }

    /**
     * Verify that we can request selected statistics split into separate
     * requests.
     */
    public function testGetStatisticsSelectedSplit()
    {
        // Mock the API result
        $this->mockFunctions['curl_exec']->expects($this->exactly(2))
            ->will($this->onConsecutiveCalls(
                '{"total":1234,"last24h":123,"saleVelocityPerSeconds":0.012345678}',
                '{"total":123456,"last24h":789,"saleVelocityPerSeconds":0.033333333}'
            ))
        ;

        // Get the client
        $client = GHTMojangApiClient::create(GHTMojangApiClient::SUBDOMAIN_API);

        // Verify the expected result
        $this->assertEquals(
            array(
                'item_sold_cobalt' => array('total' => '1234', 'last24h' => '123', 'saleVelocityPerSeconds' => '0.012345678'),
                'item_sold_scrolls' => array('total' => '123456', 'last24h' => '789', 'saleVelocityPerSeconds' => '0.033333333'),
            ),
            $client->getStatistics(array('item_sold_cobalt', 'item_sold_scrolls'), true)
        );
    }

    /**
     * Verify that we can request the status of Mojang services.
     */
    public function testGetStatus()
    {
        // Configure the expected result
        $expected = array(
            'minecraft.net' => 'yellow',
            'session.minecraft.net' => 'green',
            'account.mojang.com' => 'green',
            'auth.mojang.com' => 'green',
            'skins.minecraft.net' => 'green',
            'authserver.mojang.com' => 'green',
            'sessionserver.mojang.com' => 'yellow',
            'api.mojang.com' => 'green',
            'textures.minecraft.net' => 'red',
            'mojang.com' => 'green',
        );

        // Mock the API result
        $this->mockFunctions['curl_exec']->expects($this->once())
            ->will($this->returnValue(json_encode($expected)))
        ;

        // Get the client
        $client = GHTMojangApiClient::create(GHTMojangApiClient::SUBDOMAIN_STATUS);

        // Verify the expected result
        $this->assertEquals($expected, $client->getStatus());
    }

    /**
     * Verify that information for the current authenticated user can be
     * requested.
     */
    public function testGetUserInformation()
    {
        // Get the client
        $credentials = new Credentials();
        $credentials->setToken('testOauthToken');
        $client = GHTMojangApiClient::create(GHTMojangApiClient::SUBDOMAIN_API, $credentials);

        // Confirm that no exception is thrown
        $client->getUserInformation();
        $this->assertTrue(true);
    }

    /**
     * Verify that attempting a skin reset request with an invalid connector
     * throws an exception.
     */
    public function testGetUserInformationWithoutOauth()
    {
        // Expect the exception
        $this->expectException(\BadMethodCallException::class);
        $this->expectExceptionMessage('The "getUserInformation" method requires an authentication token.');

        // Get the client
        $client = GHTMojangApiClient::create(GHTMojangApiClient::SUBDOMAIN_API);

        // Trigger the exception
        $client->getUserInformation();
    }

    /**
     * Verify that the current authentication token can be invalidated.
     */
    public function testInvalidate()
    {
        // Mock the API result
        $this->mockFunctions['curl_exec']->expects($this->once())
            ->will($this->returnValue(''))
        ;
        $this->mockFunctions['curl_getinfo']->expects($this->once())
            ->will($this->returnValue(204))
        ;

        // Get the client
        $client = GHTMojangApiClient::create(GHTMojangApiClient::SUBDOMAIN_OAUTH);
        $client->getCredentials()
            ->setClientToken('testClientToken')
            ->setProfileId('testUuid')
            ->setProfileName('testName')
            ->setToken('testAccessToken')
        ;

        // Confirm that no exception is thrown, and the token is wiped out
        $client->invalidate();
        $this->assertEquals('', $client->getCredentials()->getToken());
    }

    /**
     * Verify that a failed authentication invalidation attempt throws an
     * exception.
     */
    public function testInvalidateFailure()
    {
        // Expect the exception
        $this->expectException(MojangApiException::class);
        $this->expectExceptionMessage('Invalid token.');

        // Mock the API result
        $this->mockFunctions['curl_exec']->expects($this->once())
            ->will($this->returnValue('{"error":"ForbiddenOperationException","errorMessage":"Invalid token."}'))
        ;
        $this->mockFunctions['curl_getinfo']->expects($this->once())
            ->will($this->returnValue(403))
        ;

        // Get the client
        $client = GHTMojangApiClient::create(GHTMojangApiClient::SUBDOMAIN_OAUTH);

        // Trigger the exception
        $client->invalidate();
    }

    /**
     * Verify that authentication can be refreshed.
     */
    public function testRefresh()
    {
        // Mock the API result
        $this->mockFunctions['curl_exec']->expects($this->once())
            ->will($this->returnValue('{"accessToken":"testFreshAccessToken","clientToken":"testClientToken","selectedProfile":{"id":"testUuid","name":"testName"}}'))
        ;

        // Get the client
        $client = GHTMojangApiClient::create(GHTMojangApiClient::SUBDOMAIN_OAUTH);
        $client->getCredentials()
            ->setClientToken('testClientToken')
            ->setProfileId('testUuid')
            ->setProfileName('testName')
            ->setToken('testOldAccessToken')
        ;

        // Verify the expected result
        $this->assertEquals(
            array(
                'accessToken' => 'testFreshAccessToken',
                'clientToken' => 'testClientToken',
                'selectedProfile' => array(
                    'id' => 'testUuid',
                    'name' => 'testName',
                ),
            ),
            $client->refresh()
        );
        $this->assertEquals('testClientToken', $client->getCredentials()->getClientToken());
        $this->assertEquals('testFreshAccessToken', $client->getCredentials()->getToken());
    }

    /**
     * Verify that a skin reset can be requested.
     */
    public function testResetSkin()
    {
        // Get the client
        $credentials = new Credentials();
        $credentials->setToken('testOauthToken');
        $client = GHTMojangApiClient::create(GHTMojangApiClient::SUBDOMAIN_API, $credentials);

        // Confirm that no exception is thrown
        $client->resetSkin('testUUID');
        $this->assertTrue(true);
    }

    /**
     * Verify that attempting a skin reset request with an invalid connector
     * throws an exception.
     */
    public function testResetSkinWithoutOauth()
    {
        // Expect the exception
        $this->expectException(\BadMethodCallException::class);
        $this->expectExceptionMessage('The "resetSkin" method requires an authentication token.');

        // Get the client
        $client = GHTMojangApiClient::create(GHTMojangApiClient::SUBDOMAIN_API);

        // Trigger the exception
        $client->resetSkin('testUUID');
    }

    /**
     * Verify that the current authentication token can be invalidated by
     * signing out using the login name and password.
     */
    public function testSignOut()
    {
        // Mock the API result
        $this->mockFunctions['curl_exec']->expects($this->once())
            ->will($this->returnValue(''))
        ;
        $this->mockFunctions['curl_getinfo']->expects($this->once())
            ->will($this->returnValue(204))
        ;

        // Get the client
        $client = GHTMojangApiClient::create(GHTMojangApiClient::SUBDOMAIN_OAUTH);
        $client->getCredentials()
            ->setClientToken('testClientToken')
            ->setProfileId('testUuid')
            ->setProfileName('testName')
            ->setToken('testAccessToken')
        ;

        // Confirm that no exception is thrown, and the token is wiped out
        $client->signOut('testName', 'testPassword');
        $this->assertEquals('', $client->getCredentials()->getToken());
    }

    /**
     * Verify that a failed authentication signout attempt throws an
     * exception.
     */
    public function testSignOutFailure()
    {
        // Expect the exception
        $this->expectException(MojangApiException::class);
        $this->expectExceptionMessage('Invalid credentials. Invalid username or password.');

        // Mock the API result
        $this->mockFunctions['curl_exec']->expects($this->once())
            ->will($this->returnValue('{"error":"ForbiddenOperationException","errorMessage":"Invalid credentials. Invalid username or password."}'))
        ;
        $this->mockFunctions['curl_getinfo']->expects($this->once())
            ->will($this->returnValue(403))
        ;

        // Get the client
        $client = GHTMojangApiClient::create(GHTMojangApiClient::SUBDOMAIN_OAUTH);

        // Trigger the exception
        $client->signOut('testName', 'testWrongPassword');
    }

    /**
     * Verify that a skin can be uploaded.
     */
    public function testUploadSkin()
    {
        // Mock the file system methods
        $isFileMethod = $this->getFunctionMock('GHT\MojangApiClient', 'is_file');
        $isFileMethod->expects($this->once())
            ->with('/tmp/test.png')
            ->will($this->returnValue(true))
        ;
        $mimeContentTypeMethod = $this->getFunctionMock('GHT\MojangApiClient', 'mime_content_type');
        $mimeContentTypeMethod->expects($this->once())
            ->with('/tmp/test.png')
            ->will($this->returnValue('image/png'))
        ;

        // Get the client
        $credentials = new Credentials();
        $credentials->setToken('testOauthToken');
        $client = GHTMojangApiClient::create(GHTMojangApiClient::SUBDOMAIN_API, $credentials);

        // Confirm that no exception is thrown
        $client->uploadSkin('testUUID', '/tmp/test.png', 'slim');
        $this->assertTrue(true);
    }

    /**
     * Verify that attempting to upload a skin with a broken file path throws an
     * exception.
     */
    public function testUploadSkinBadFilePath()
    {
        // Expect the exception
        $this->expectException(\InvalidArgumentException::class);
        $this->expectExceptionMessage('"/tmp/does-not-exist.png" is not a valid file or file not found.');

        // Mock the file system methods
        $isFileMethod = $this->getFunctionMock('GHT\MojangApiClient', 'is_file');
        $isFileMethod->expects($this->once())
            ->with('/tmp/does-not-exist.png')
            ->will($this->returnValue(false))
        ;

        // Get the client
        $credentials = new Credentials();
        $credentials->setToken('testOauthToken');
        $client = GHTMojangApiClient::create(GHTMojangApiClient::SUBDOMAIN_API, $credentials);

        // Trigger the exception
        $client->uploadSkin('testUUID', '/tmp/does-not-exist.png', 'slim');
    }

    /**
     * Verify that attempting to upload a skin with an invalid model name throws
     * an exception.
     */
    public function testUploadSkinWithInvalidModel()
    {
        // Expect the exception
        $this->expectException(\InvalidArgumentException::class);
        $this->expectExceptionMessage('"smooth" is not a valid skin model name.  Expected one of:');

        // Mock the file system methods
        $isFileMethod = $this->getFunctionMock('GHT\MojangApiClient', 'is_file');
        $isFileMethod->expects($this->once())
            ->with('/tmp/test.png')
            ->will($this->returnValue(true))
        ;

        // Get the client
        $credentials = new Credentials();
        $credentials->setToken('testOauthToken');
        $client = GHTMojangApiClient::create(GHTMojangApiClient::SUBDOMAIN_API, $credentials);

        // Trigger the exception
        $client->uploadSkin('testUUID', '/tmp/test.png', 'smooth');
    }

    /**
     * Verify that attempting to upload a skin with an invalid connector throws
     * an exception.
     */
    public function testUploadSkinWithoutOauth()
    {
        // Expect the exception
        $this->expectException(\BadMethodCallException::class);
        $this->expectExceptionMessage('The "uploadSkin" method requires an authentication token.');

        // Get the client
        $client = GHTMojangApiClient::create(GHTMojangApiClient::SUBDOMAIN_API);

        // Trigger the exception
        $client->uploadSkin('testUUID', '/tmp/test.png', 'slim');
    }

    /**
     * Verify that the current authentication token can be validated.
     */
    public function testValidate()
    {
        // Mock the API result
        $this->mockFunctions['curl_exec']->expects($this->once())
            ->will($this->returnValue(''))
        ;
        $this->mockFunctions['curl_getinfo']->expects($this->once())
            ->will($this->returnValue(204))
        ;

        // Get the client
        $client = GHTMojangApiClient::create(GHTMojangApiClient::SUBDOMAIN_OAUTH);
        $client->getCredentials()
            ->setClientToken('testClientToken')
            ->setProfileId('testUuid')
            ->setProfileName('testName')
            ->setToken('testAccessToken')
        ;

        // Confirm that no exception is thrown
        $client->validate();
        $this->assertTrue(true);
    }

    /**
     * Verify that a failed authentication validation attempt throws an
     * exception.
     */
    public function testValidateFailure()
    {
        // Expect the exception
        $this->expectException(MojangApiException::class);
        $this->expectExceptionMessage('Invalid token.');

        // Mock the API result
        $this->mockFunctions['curl_exec']->expects($this->once())
            ->will($this->returnValue('{"error":"ForbiddenOperationException","errorMessage":"Invalid token."}'))
        ;
        $this->mockFunctions['curl_getinfo']->expects($this->once())
            ->will($this->returnValue(403))
        ;

        // Get the client
        $client = GHTMojangApiClient::create(GHTMojangApiClient::SUBDOMAIN_OAUTH);

        // Trigger the exception
        $client->validate();
    }
}
