<?php

namespace GHT\MojangApiClient;

use GHT\ApiClient\Entity\ApiConnector;
use GHT\ApiClient\Entity\OauthConnector;
use GHT\ApiClient\GHTApiClient;
use GHT\MojangApiClient\Authentication\Credentials;
use GHT\MojangApiClient\Exception\MojangApiException;

/**
 * GHT Mojang API Client.
 */
class GHTMojangApiClient extends GHTApiClient
{
    /**
     * @var string API base URL
     */
    const BASE_URL = 'https://%s.mojang.com';

    /**
     * @var string Authentication agency via Minecraft
     */
    const GAME_MINECRAFT = 'Minecraft';

    /**
     * @var string Authentication agency via Scrolls
     */
    const GAME_SCROLLS = 'Scrolls';

    /**
     * @var string API path for authenticating a user
     */
    const PATH_AUTHENTICATE = 'authenticate';

    /**
     * @var string API path force an authentication token to be invalid
     */
    const PATH_AUTHENTICATION_INVALIDATE = 'invalidate';

    /**
     * @var string API path for refreshing authentication token
     */
    const PATH_AUTHENTICATION_REFRESH = 'refresh';

    /**
     * @var string API path for ending an authenticated session
     */
    const PATH_AUTHENTICATION_SIGNOUT = 'signout';

    /**
     * @var string API path for validating authentication token
     */
    const PATH_AUTHENTICATION_VALIDATE = 'validate';

    /**
     * @var string API path for Blocked Servers
     */
    const PATH_BLOCKED_SERVERS = 'blockedservers';

    /**
     * @var string API path for Change Skin
     */
    const PATH_CHANGE_SKIN = 'user/profile/%s/skin';

    /**
     * @var string API path for UUIDs and other profile data for user names
     */
    const PATH_MINECRAFT_PROFILES = 'profiles/minecraft';

    /**
     * @var string API path for Reset Skin
     */
    const PATH_RESET_SKIN = 'user/profile/%s/skin';

    /**
     * @var string API path for Statistics
     */
    const PATH_STATISTICS = 'orders/statistics';

    /**
     * @var string API path for Status
     */
    const PATH_STATUS = 'check';

    /**
     * @var string API path for Upload Skin
     */
    const PATH_UPLOAD_SKIN = 'user/profile/%s/skin';

    /**
     * @var string API path for User Info
     */
    const PATH_USER_INFO = 'user';

    /**
     * @var string API path for Username -> UUID at time
     */
    const PATH_USERNAME_TO_UUID_AT_TIME = 'users/profiles/minecraft/%s?at=%s';

    /**
     * @var string API path for UUID -> Profile + Skin/Cape
     */
    const PATH_UUID_TO_USERNAME_PLUS = 'session/minecraft/profile/%s';

    /**
     * @var string API path for UUID -> Name history
     */
    const PATH_UUID_USERNAME_HISTORY = 'user/profiles/%s/names';

    /**
     * @var string Slim skin model
     */
    const SKIN_MODEL_SLIM = 'slim';

    /**
     * @var string Standard skin model
     */
    const SKIN_MODEL_STANDARD = 'standard';

    /**
     * @var string Cobalt sold statistic
     */
    const STATISTIC_COBALT_SOLD = 'item_sold_cobalt';

    /**
     * @var string Minecraft prepaid card redeemed statistic
     */
    const STATISTIC_MINECRAFT_CARD_REDEEMED = 'prepaid_card_redeemed_minecraft';

    /**
     * @var string Minecraft sold statistic
     */
    const STATISTIC_MINECRAFT_SOLD = 'item_sold_minecraft';

    /**
     * @var string Scrolls sold statistic
     */
    const STATISTIC_SCROLLS_SOLD = 'item_sold_scrolls';

    /**
     * @var string Default API subdomain
     */
    const SUBDOMAIN_API = 'api';

    /**
     * @var string Authentication API subdomain
     */
    const SUBDOMAIN_OAUTH = 'authserver';

    /**
     * @var string Session API subdomain
     */
    const SUBDOMAIN_SESSION = 'sessionserver';

    /**
     * @var string Status API subdomain
     */
    const SUBDOMAIN_STATUS = 'status';

    /**
     * @var \GHT\MojangApiClient\Authentication\Credentials
     */
    protected $credentials;

    /**
     * Authenticate a user with an account email and password.  For unmigrated
     * accounts, provide the user name in place of the email.
     *
     * @param string $login The account email or user name.
     * @param string $password The account password.
     * @param string $agent The game related to the authentication.
     *
     * @return array
     */
    public function authenticate($login, $password, $agent = null)
    {
        // Must be called on the authentication API
        $this->validateApi(self::SUBDOMAIN_OAUTH);

        // If set, validate the game agent
        if ($agent && !in_array($agent, self::getGameAgents())) {
            throw new \InvalidArgumentException(sprintf(
                '"%s" is not a valid game name.  Expected one of: %s',
                $agent,
                implode(', ', self::getGameAgents())
            ));
        }

        // Set the payload
        $apiData = array(
            'clientToken' => $this->credentials->getClientToken(),
            'password' => $password,
            'username' => $login,
        );

        if ($agent) {
            $apiData['agent'] = array(
                'name' => $agent,
                'version' => 1
            );
        }

        // Submit authentication credentials
        $response = $this->post(self::PATH_AUTHENTICATE, $apiData);

        // Return result or error
        if ($this->getResponseCode() === 200) {
            $responseData = json_decode($response, true);

            // Transport the selected profile UUID and name if returned
            if (!empty($responseData['selectedProfile'])) {
                $this->credentials->setProfileId($responseData['selectedProfile']['id'])
                    ->setProfileName($responseData['selectedProfile']['name'])
                    ->setToken($responseData['accessToken'])
                ;
            }

            return $responseData;
        }
        else {
            throw new MojangApiException($response);
        }
    }

    /**
     * Change a profile's skin by UUID.
     *
     * @param $uuid The UUID of the user changing skin.
     * @param $url The URL location of the skin file.
     * @param $model The skin model type.
     */
    public function changeSkin($uuid, $url, $model = self::SKIN_MODEL_STANDARD)
    {
        // Must be called on the default API with OAuth2 authentication
        $this->validateApi(self::SUBDOMAIN_API, true);

        // Validate the skin model name
        if (!in_array($model, self::getSkinModelNames())) {
            throw new \InvalidArgumentException(sprintf(
                '"%s" is not a valid skin model name.  Expected one of: %s',
                $model,
                implode(', ', self::getSkinModelNames())
            ));
        }

        // Set the post path and payload
        $apiPath = sprintf(self::PATH_CHANGE_SKIN, self::cleanUuid($uuid));
        $apiData = array(
            'model' => ($model === self::SKIN_MODEL_STANDARD) ? '' : $model,
            'url' => $url,
        );

        // Change the skin
        if ($error = $this->post($apiPath, $apiData)) {
            throw new MojangApiException($error);
        }
    }

    /**
     * Make sure a UUID is clean.
     *
     * @param string $uuid The potentially hyphenated UUID.
     *
     * @return string
     */
    public static function cleanUuid($uuid)
    {
        return preg_replace('/[^a-z0-9]/i', '', $uuid);
    }

    /**
     * Create a new instance.  The specific API subdomain must be specified.
     *
     * @param string $apiName The specific Mojang API identifier.
     * @param \GHT\MojangApiClient\Authentication\Credentials $credentials The
     *        authentication credentials.
     *
     * @return MojangApiClient
     */
    public static function create($apiName, Credentials $credentials = null)
    {
        // Validate the API name
        if (!in_array($apiName, self::getApiNames())) {
            throw new \InvalidArgumentException(sprintf(
                '"%s" is not a valid API subdomain name.  Expected one of: %s',
                $apiName,
                implode(', ', self::getApiNames())
            ));
        }

        // Create a connector targeting the modified base URL
        if ($credentials) {
            $connector = new OauthConnector(sprintf(self::BASE_URL, $apiName), $credentials->getToken());
        }
        else {
            $connector = new ApiConnector(sprintf(self::BASE_URL, $apiName));
        }

        $client = new self($connector);

        // If an authentication client, create new credentials
        if ($apiName === self::SUBDOMAIN_OAUTH) {
            $client->generateCredentials();
        }
        // Otherwise, transport any provided credentials with the client
        elseif ($credentials) {
            $client->setCredentials($credentials);
        }

        return $client;
    }

    /**
     * Wrapper to create a default API client.
     *
     * @param \GHT\MojangApiClient\Authentication\Credentials $credentials The
     *        authentication credentials.
     *
     * @return MojangApiClient
     */
    public static function createApiClient(Credentials $credentials = null)
    {
        return self::create(self::SUBDOMAIN_API, $credentials);
    }

    /**
     * Wrapper to create an authentication API client.
     *
     * @return MojangApiClient
     */
    public static function createAuthenticationClient()
    {
        return self::create(self::SUBDOMAIN_OAUTH);
    }

    /**
     * Wrapper to create a session API client.
     *
     * @return MojangApiClient
     */
    public static function createSessionClient()
    {
        return self::create(self::SUBDOMAIN_SESSION);
    }

    /**
     * Wrapper to create a status API client.
     *
     * @return MojangApiClient
     */
    public static function createStatusClient()
    {
        return self::create(self::SUBDOMAIN_STATUS);
    }

    /**
     * Generate a fresh set of client credentials.
     */
    public function generateCredentials()
    {
        // We only want to generate credentials once
        if ($this->credentials instanceof Credentials) {
            return;
        }

        // Must be called on the authentication API
        $this->validateApi(self::SUBDOMAIN_OAUTH);

        $this->credentials = new Credentials();
    }

    /**
     * Get all valid Mojang API subdomain names.
     *
     * @return array
     */
    public static function getApiNames()
    {
        return array(
            self::SUBDOMAIN_API,
            self::SUBDOMAIN_OAUTH,
            self::SUBDOMAIN_SESSION,
            self::SUBDOMAIN_STATUS,
        );
    }

    /**
     * Get blocked server hashes.
     *
     * @return array
     */
    public function getBlockedServerHashes()
    {
        // Must be called on the session API
        $this->validateApi(self::SUBDOMAIN_SESSION);

        // Get the blocked server hashes
        $hashes = $this->get(self::PATH_BLOCKED_SERVERS);

        // Return as an array
        return explode("\n", $hashes);
    }

    /**
     * Get the authentication credentials.
     *
     * @return \GHT\MojangApiClient\Authentication\Credentials
     */
    public function getCredentials()
    {
        // Must be called on the authentication API
        $this->validateApi(self::SUBDOMAIN_OAUTH);

        return $this->credentials;
    }

    /**
     * Get all valid game authentication agent names.
     *
     * @return array
     */
    public static function getGameAgents()
    {
        return array(
            self::GAME_MINECRAFT,
            self::GAME_SCROLLS,
        );
    }

    /**
     * Get all names ever used by a profile.
     *
     * @param string $uuid The UUID of the profile.
     *
     * @return array
     */
    public function getNames($uuid)
    {
        // Must be called on the session API
        $this->validateApi(self::SUBDOMAIN_API);

        // Request the profile names
        $names = $this->get(sprintf(self::PATH_UUID_USERNAME_HISTORY, self::cleanUuid($uuid)));

        // Return as an array
        return json_decode($names, true);
    }

    /**
     * Get profile data for a UUID.
     *
     * @param string $uuid The UUID of the profile.
     *
     * @return array
     */
    public function getProfile($uuid)
    {
        // Must be called on the session API
        $this->validateApi(self::SUBDOMAIN_SESSION);

        // Request the profile data
        $profile = $this->get(sprintf(self::PATH_UUID_TO_USERNAME_PLUS, self::cleanUuid($uuid)));

        $profileData = json_decode($profile, true);
        if (empty($profileData['properties'])) {
            return $profileData;
        }

        // Decode the profile properties
        foreach ($profileData['properties'] as $key => $data) {
            if (empty($data['value'])) {
                continue 1;
            }
            $data['value'] = json_decode(base64_decode($data['value']), true);
            $profileData['properties'][$key] = $data;
        }

        return $profileData;
    }

    /**
     * Get profile data for a user name at a given time.
     *
     * @param string $name The user name.
     * @param \DateTime $when The date and time.
     *
     * @return array
     */
    public function getProfileForName($name, $when = null)
    {
        // Must be called on the default API
        $this->validateApi(self::SUBDOMAIN_API);

        // Validate when
        $when = $when ? $when : new \DateTime();
        if (!($when instanceof \DateTime)) {
            throw new \InvalidArgumentException(sprintf('Attempting to get profile for name, expected DateTime object, got %s.', gettype($when)));
        }

        // Request the profile data
        $profile = $this->get(sprintf(self::PATH_USERNAME_TO_UUID_AT_TIME, $name, $when->getTimestamp()));

        // Return as an array
        return json_decode($profile, true);
    }

    /**
     * Get profile data for user names.
     *
     * @param array $names The user names.
     *
     * @return array
     */
    public function getProfilesForNames(array $names)
    {
        // Must be called on the default API
        $this->validateApi(self::SUBDOMAIN_API);

        // Clean out empty names to avoid an IllegalArgumentException response
        $cleanNames = array_filter($names, function($value) {
            return (!is_null($value) && $value !== '');
        });

        // Request the profile data
        $profiles = $this->post(self::PATH_MINECRAFT_PROFILES, $cleanNames);

        // Return as an array
        return json_decode($profiles, true);
    }

    /**
     * Get all valid skin model names.
     *
     * @return array
     */
    public static function getSkinModelNames()
    {
        return array(
            self::SKIN_MODEL_SLIM,
            self::SKIN_MODEL_STANDARD,
        );
    }

    /**
     * Get all valid statistics metric keys.
     *
     * @return array
     */
    public static function getStatisticsKeys()
    {
        return array(
            self::STATISTIC_COBALT_SOLD,
            self::STATISTIC_MINECRAFT_CARD_REDEEMED,
            self::STATISTIC_MINECRAFT_SOLD,
            self::STATISTIC_SCROLLS_SOLD,
        );
    }

    /**
     * Get the current Minecraft statistics.  Specify the metric key or an
     * array of metric keys to request only those statistics, otherwise all
     * available statistics are requested.
     *
     * By default, statistics are aggregated.  To request separate statistics
     * for each metric, set the $split flag to true.  WARNING: a separate API
     * request is made for each specified metric, affecting the rate at which
     * your application may reach the number of requests limit.  Result will be
     * an array of statistics arrays keyed by each metric key.
     *
     * @param string|array<string> $metrics The metric key or keys.
     * @param boolean $split Set to true to get split metrics, not aggregate.
     *
     * @return array
     */
    public function getStatistics($metrics = null, $split = null)
    {
        // Must be called on the default API
        $this->validateApi(self::SUBDOMAIN_API);

        // Encode the metric keys for the request or requests
        $metrics = $metrics ? (array) $metrics : self::getStatisticsKeys();

        if ($split) {
            array_walk($metrics, function(&$value) {
                $value = array('metricKeys' => (array) $value);
            });
        }
        else {
            $metrics = array('metricKeys' => $metrics);
        }

        // Request the statistics
        $statistics = array();
        foreach ($metrics as $requestData) {
            $response = $this->post(self::PATH_STATISTICS, $requestData);

            // Collect the response
            if ($split) {
                // There will be only one metric key per request
                $statistics[$requestData['metricKeys'][0]] = json_decode($response, true);
            }
            else {
                // There will be only one request with all metrics
                $statistics = json_decode($response, true);
            }
        }

        return $statistics;
    }

    /**
     * Get the current status of Mojang services.
     *
     * @return array
     */
    public function getStatus()
    {
        // Must be called on the status API
        $this->validateApi(self::SUBDOMAIN_STATUS);

        // Request the statuses
        $statuses = $this->get(self::PATH_STATUS);

        // Return as an array
        return json_decode($statuses, true);
    }

    /**
     * Get information for the current user.
     *
     * @return array
     */
    public function getUserInformation()
    {
        // Must be called on the default API with OAuth2 authentication
        $this->validateApi(self::SUBDOMAIN_API, true);

        // Request the user information
        $information = $this->get(self::PATH_USER_INFO);

        // Return as an array
        return json_decode($information, true);
    }

    /**
     * Invalidate authentication token.
     */
    public function invalidate()
    {
        // Must be called on the authentication API
        $this->validateApi(self::SUBDOMAIN_OAUTH);

        // Set the payload
        $apiData = array(
            'accessToken' => $this->credentials->getToken(),
            'clientToken' => $this->credentials->getClientToken(),
        );

        // Invalidate the authentication credentials
        $error = $this->post(self::PATH_AUTHENTICATION_INVALIDATE, $apiData);
        if ($error || $this->getResponseCode() !== 204) {
            throw new MojangApiException($error);
        }

        // Destroy the credentials token
        $this->credentials->setToken('');
    }

    /**
     * Refresh authentication token.
     *
     * @return array
     */
    public function refresh()
    {
        // Must be called on the authentication API
        $this->validateApi(self::SUBDOMAIN_OAUTH);

        // Set the payload
        $apiData = array(
            'accessToken' => $this->credentials->getToken(),
            'clientToken' => $this->credentials->getClientToken(),
            'selectedProfile' => array(
                'id' => $this->credentials->getProfileId(),
                'name' => $this->credentials->getProfileName(),
            ),
        );

        // Request the fresh authentication credentials
        $refreshed = $this->post(self::PATH_AUTHENTICATION_REFRESH, $apiData);

        // Update the credentials
        $refreshedData = json_decode($refreshed, true);
        $this->credentials->setToken($refreshedData['accessToken']);

        // Return as an array
        return $refreshedData;
    }

    /**
     * Reset a profile's skin by UUID.
     *
     * @param string $uuid The UUID of the profile.
     */
    public function resetSkin($uuid)
    {
        // Must be called on the default API with OAuth2 authentication
        $this->validateApi(self::SUBDOMAIN_API, true);

        // Set the post path
        $apiPath = sprintf(self::PATH_RESET_SKIN, self::cleanUuid($uuid));

        // Reset the skin
        if ($error = $this->delete($apiPath)) {
            throw new MojangApiException($error);
        }
    }

    /**
     * Set the client credentials.
     *
     * @param \GHT\MojangApiClient\Authentication\Credentials The
     *        authentication credentials.
     */
    public function setCredentials(Credentials $credentials)
    {
        // Must be called on the default API
        $this->validateApi(self::SUBDOMAIN_API);

        $this->credentials = $credentials;
    }

    /**
     * Invalidate authentication token using login name and password.
     *
     * @param string $login The account email or user name.
     * @param string $paasword The account password.
     */
    public function signOut($login, $password)
    {
        // Must be called on the authentication API
        $this->validateApi(self::SUBDOMAIN_OAUTH);

        // Set the payload
        $apiData = array(
            'password' => $password,
            'username' => $login,
        );

        // Invalidate the authentication credentials
        $error = $this->post(self::PATH_AUTHENTICATION_INVALIDATE, $apiData);
        if ($error || $this->getResponseCode() !== 204) {
            throw new MojangApiException($error);
        }

        // Destroy the credentials token
        $this->credentials->setToken('');
    }

    /**
     * Upload a new skin for a profile by UUID.
     *
     * @param string $uuid The UUID of the profile.
     * @param string $pathName The path and name of the skin file.
     * @param $model The skin model type.
     */
    public function uploadSkin($uuid, $pathName, $model = self::SKIN_MODEL_STANDARD)
    {
        // Must be called on the default API with OAuth2 authentication
        $this->validateApi(self::SUBDOMAIN_API, true);

        // Must be a valid file
        if (!is_file($pathName)) {
            throw new \InvalidArgumentException(sprintf('"%s" is not a valid file or file not found.', $pathName));
        }

        // Validate the skin model name
        if (!in_array($model, self::getSkinModelNames())) {
            throw new \InvalidArgumentException(sprintf(
                '"%s" is not a valid skin model name.  Expected one of: %s',
                $model,
                implode(', ', self::getSkinModelNames())
            ));
        }

        // Create the file request object
        $file = new \CURLFile($pathName, mime_content_type($pathName));

        // Set the post path and payload
        $apiPath = sprintf(self::PATH_UPLOAD_SKIN, self::cleanUuid($uuid));
        $apiData = array(
            'file' => $file,
            'model' => ($model === self::SKIN_MODEL_STANDARD) ? '' : $model,
        );

        // Upload the skin
        if ($error = $this->put($apiPath, $apiData)) {
            throw new MojangApiException($error);
        }
    }

    /**
     * Validate authentication token.
     */
    public function validate()
    {
        // Must be called on the authentication API
        $this->validateApi(self::SUBDOMAIN_OAUTH);

        // Set the payload
        $apiData = array(
            'accessToken' => $this->credentials->getToken(),
            'clientToken' => $this->credentials->getClientToken(),
        );

        // Validate the authentication credentials
        $error = $this->post(self::PATH_AUTHENTICATION_VALIDATE, $apiData);
        if ($error || $this->getResponseCode() !== 204) {
            throw new MojangApiException($error);
        }
    }

    /**
     * Validate the current API subdomain target.
     *
     * @param string $apiName The API name to validate.
     * @param boolean $oauth Set to true to validate OAuth2 connector.
     *
     * @throws \BadMethodCallException
     */
    public function validateApi($apiName, $oauth = false)
    {
        // Validate the connector host based on the given API name
        if ($this->connector->getHost() !== sprintf(self::BASE_URL, $apiName)) {
            $method = debug_backtrace()[1]['function'];

            throw new \BadMethodCallException(sprintf(
                '"%s" is not a method of the %s API.',
                $method,
                $this->connector->getHost()
            ));
        }

        // If not OAuth2, there's no further validation
        if (!$oauth) {
            return;
        }

        // Validate the connector has an OAuth2 token
        if (!($this->connector instanceof OauthConnector) || !$this->connector->hasAuthentication()) {
            $method = debug_backtrace()[1]['function'];

            throw new \BadMethodCallException(sprintf(
                'The "%s" method requires an authentication token.',
                $method
            ));
        }
    }
}
