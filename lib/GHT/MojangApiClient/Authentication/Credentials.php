<?php

namespace GHT\MojangApiClient\Authentication;

/**
 * Provides transport utility for Mojang OAuth2 authentication credentials.
 */
class Credentials
{
    /**
     * @var string
     */
    protected $clientToken;

    /**
     * @var string
     */
    protected $profileId;

    /**
     * @var string
     */
    protected $profileName;

    /**
     * @var string
     */
    protected $token;

    /**
     * The constructor.
     */
    public function __construct()
    {
        $this->clientToken = bin2hex(random_bytes(20));
    }

    /**
     * Get the clientToken.
     *
     * @return string
     */
    public function getClientToken()
    {
        return $this->clientToken;
    }

    /**
     * Get the profileId.
     *
     * @return string
     */
    public function getProfileId()
    {
        return $this->profileId;
    }

    /**
     * Get the profileName.
     *
     * @return string
     */
    public function getProfileName()
    {
        return $this->profileName;
    }

    /**
     * Get the token.
     *
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * Set the clientToken.
     *
     * @param string $clientToken
     *
     * @return Credentials
     */
    public function setClientToken($clientToken)
    {
        $this->clientToken = $clientToken;

        return $this;
    }

    /**
     * Set the profileId.
     *
     * @param string $profileId
     *
     * @return Credentials
     */
    public function setProfileId($profileId)
    {
        $this->profileId = $profileId;

        return $this;
    }

    /**
     * Set the profileName.
     *
     * @param string $profileName
     *
     * @return Credentials
     */
    public function setProfileName($profileName)
    {
        $this->profileName = $profileName;

        return $this;
    }

    /**
     * Set the token.
     *
     * @param string $token
     *
     * @return Credentials
     */
    public function setToken($token)
    {
        $this->token = $token;

        return $this;
    }
}
