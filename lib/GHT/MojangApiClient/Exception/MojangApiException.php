<?php

namespace GHT\MojangApiClient\Exception;

/**
 * Exception for transporting Mojang JSON error responses.
 */
class MojangApiException extends \Exception
{
    /**
     * {@inheritdoc}
     */
    public function __construct($message = '', $code = 0, $previous = null)
    {
        // If no message is set, just instantiate the exception
        if (!$message) {
            parent::__construct($message, $code, $previous);
            return;
        }

        // Otherwise, attempt to parse the error message
        $messageDecoded = json_decode($message, true);
        $messageParsed = sprintf(
            '%s: %s',
            !empty($messageDecoded['error']) ? $messageDecoded['error'] : 'Unknown Error',
            !empty($messageDecoded['errorMessage']) ? $messageDecoded['errorMessage'] : sprintf('API response :: %s', $message)
        );

        // Instantiate the exception with the parsed error message
        parent::__construct($messageParsed, $code, $previous);
    }
}
